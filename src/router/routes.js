
const routes = [
  { path: '/login', name: 'Login', component: () => import('pages/Login.vue') },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', redirect: '/cards' },
      { path: '/chat', component: () => import('pages/ChatRoom.vue') },
      { path: '/cards', component: () => import('pages/Index.vue') },
      { path: '/list', name: 'UserList', component: () => import('pages/List.vue') },
      { path: '/list/create', name: 'UserCreate', component: () => import('pages/List.vue') },
      { path: '/list/:id', name: 'UserShow', component: () => import('pages/List.vue'), props: route => ({ userId: parseInt(route.params.id) }) },
      { path: '/list/:id/edit', name: 'UserEdit', component: () => import('pages/List.vue'), props: route => ({ userId: parseInt(route.params.id) }) }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
