export function setUsers (context, users) {
  context.commit('setUsers', users)
}

export function updateUser (context, user) {
  context.commit('updateUser', user)
}

export function createUser (context, user) {
  context.commit('createUser', user)
}

export function deleteUser (context, userId) {
  context.commit('deleteUser', userId)
}
