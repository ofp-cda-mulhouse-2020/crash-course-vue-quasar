export function getUser (state) {
  return (id) => state.users.find(u => id === u.id)
}

export function getUsers (state) {
  return state.users
}
