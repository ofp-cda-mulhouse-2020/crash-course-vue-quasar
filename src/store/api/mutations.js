import Vue from 'vue'

export function setUsers (state, users) {
  Vue.set(state, 'users', users)
}

export function updateUser (state, user) {
  const idx = state.users.findIndex(u => u.id === user.id)
  if (idx >= 0) Vue.set(state.users, idx, Object.assign({}, user))
}

export function createUser (state, user) {
  state.users.push(Object.assign({}, user))
}

export function deleteUser (state, userId) {
  const idx = state.users.findIndex(u => u.id === userId)
  state.users.splice(idx, 1)
}
