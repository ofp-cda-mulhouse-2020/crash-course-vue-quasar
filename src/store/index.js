import Vue from 'vue'
import Vuex from 'vuex'

import api from './api'
import auth from './auth'

Vue.use(Vuex)

const Store = new Vuex.Store({
  modules: {
    api,
    auth
  },

  // enable strict mode (adds overhead!)
  // for dev mode only
  strict: process.env.DEBUGGING
})

export default Store
