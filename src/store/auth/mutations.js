export function setToken (state, token) {
  state.token = token
  state.refreshToken = false
}

export function setRefreshToken (state, tokenNeedsRefresh) {
  state.refreshToken = tokenNeedsRefresh
}
