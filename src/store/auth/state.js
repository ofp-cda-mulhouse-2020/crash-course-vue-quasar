export default function () {
  return {
    refreshToken: false,
    token: window.localStorage.getItem('authToken', null)
  }
}
