import { Notify } from 'quasar'

export function login (context, token) {
  context.commit('setToken', token)
  window.localStorage.setItem('authToken', token)
  Notify.create({ message: 'Utilisateur connecté' })
}

export function logout (context) {
  context.commit('setToken', null)
  window.localStorage.removeItem('authToken')
}

export function setRefreshToken (context, tokenNeedsRefresh) {
  context.commit('setRefreshToken', tokenNeedsRefresh)
}
