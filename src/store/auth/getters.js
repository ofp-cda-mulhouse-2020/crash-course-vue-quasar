export function isAuthenticated (state) {
  return (typeof state.token) === 'string'
}

export function token (state) {
  return state.token
}

export function tokenPayload (state) {
  if (state.token === null) return { }

  try {
    const base64Url = state.token.split('.')[1]
    const base64 = base64Url.replace('-', '+').replace('_', '/')
    return JSON.parse(window.atob(base64))
  } catch (error) {
    console.log('Error decoding token')
    console.log(error)
    return {}
  }
}

export function tokenNeedsRefresh (state) {
  return state.refreshToken
}
