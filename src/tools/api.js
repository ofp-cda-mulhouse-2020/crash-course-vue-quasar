// import { Notify } from 'quasar'
import { baseUrl, clearClient, protectedClient } from 'boot/axios.js'
import Store from '../store/index'

/**
 * Main API component exposing CRUD methods for the users
 */
export default {

  /**
   * Base url of the backend
   */
  baseUrl: baseUrl,

  /**
   * Instance of axios for the clear API
   */
  clearClient: clearClient,

  /**
   * Instance of axios for the protected API
   */
  protectedClient: protectedClient,

  /**
   * Refresh the list of users
   */
  refreshUsers () {
    console.log('refresh users')

    protectedClient.get('/users')
      .then(response => {
        const users = []
        response.data.forEach(u => {
          users.push(u)
        })
        Store.dispatch('api/setUsers', users)
      })
  },

  /**
   * Login call
   */
  login (firstName, email) {
    console.log(`Trying to connect using user ${firstName}`)

    return new Promise((resolve, reject) => {
      clearClient.post('login', `firstName=${firstName}&email=${email}`)
        .then(response => {
          Store.dispatch('auth/login', response.data).then(() => {
            resolve()
          })
        }).catch(error => reject(error))
    })
  },

  /**
   * Create a new user
   */
  createUser (user) {
    console.log(`Creating user with ${JSON.stringify(user)}`)

    return new Promise((resolve, reject) => {
      protectedClient.post('/users', user)
        .then(response => {
          resolve(response.data)
        }).catch(error => reject(error))
    })
  },

  /**
   * Update a user
   */
  updateUser (user) {
    console.log(`Updating user with ${JSON.stringify(user)}`)

    return new Promise((resolve, reject) => {
      protectedClient.put(`/users/${user.id}`, user)
        .then(response => {
          resolve(response.data)
        }).catch(error => reject(error))
    })
  },

  /**
   * Delete a user
   */
  deleteUser (user) {
    console.log(`Deleting user ${JSON.stringify(user)}`)

    return protectedClient.delete(`/users/${user.id}`)
      .then(response => {
      })
  }

}
