import SockJS from 'sockjs-client'
import { Stomp } from '@stomp/stompjs'
import { Notify } from 'quasar'

import Store from '../store/index'
import API from './api'

/**
 * Remove the user from the store
 */
function onUserDeleted (msg) {
  console.log('WSEvent UserDeleted')
  Store.dispatch('api/deleteUser', msg.userId)
  Notify.create(`Utilisateur ${msg.userId} supprimé`)
}

/**
 * Create a new user in the store
 */
function onUserCreated (msg) {
  console.log('WSEvent UserCreated')
  Store.dispatch('api/createUser', msg.user)
  Notify.create(`Nouvel utilisateur créé : ${msg.user.firstName} ${msg.user.lastName}`)
}

/**
 * Update the user in the store
 */
function onUserUpdated (msg) {
  console.log('WSEvent UserUpdated')
  Store.dispatch('api/updateUser', msg.user)
  Notify.create(`Utilisateur mis à jour : ${msg.user.firstName} ${msg.user.lastName}`)
}

/**
 * Dispatch event message to the correct handler
 */
function onMessageReceived (msg) {
  if (msg.event === 'user_deleted') onUserDeleted(msg)
  else if (msg.event === 'user_created') onUserCreated(msg)
  else if (msg.event === 'user_updated') onUserUpdated(msg)
}

/**
 * Factory function to create a configrued STOMP client
 */
export default function createStompClient () {
  const url = `${API.baseUrl}/websocket`
  console.log(`Connecting websocket to ${url}`)

  const socket = new SockJS(url)
  const client = Stomp.over(socket)
  client.connect({ 'X-Authorization': 'Bearer ' + Store.state.auth.token }, function (frame) {
    console.log('WebSocket connected !')
  })

  client.onConnect = function (frame) {
    console.log('WebSocket connected !')
    Notify.create({ message: 'WebSocket connectée', timeout: 1000 })

    // Do something, all subscribes must be done is this callback
    // This is needed because this will be executed after a (re)connect
    client.subscribe('/users', (message) => {
      const wsEvent = JSON.parse(message.body)
      onMessageReceived(wsEvent)
    })
  }

  client.onStompError = function (frame) {
    // Will be invoked in case of error encountered at Broker
    // Bad login/passcode typically will cause an error
    // Complaint brokers will set `message` header with a brief message. Body may contain details.
    // Compliant brokers will terminate the connection after any error
    console.log('Broker reported error: ' + frame.headers.message)
    console.log('Additional details: ' + frame.body)
    Notify.create({ type: 'negative', message: 'Erreur de connexion WebSocket', timeout: 1000 })
  }

  return client
}
