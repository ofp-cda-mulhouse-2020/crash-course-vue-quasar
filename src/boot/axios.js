import axios from 'axios'

const baseUrl = 'http://localhost:8081'

const clearClient = axios.create({ baseURL: baseUrl })
const protectedClient = axios.create({ baseURL: baseUrl })

export { baseUrl, clearClient, protectedClient }
