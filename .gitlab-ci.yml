# Use custom image containing yarn/quasar/java/android
image: registry.gitlab.com/ofp-cda-mulhouse-2020/docker-quasar-deployment

# Stage declaration
stages:
  - build-spa
  - build-webcontainer
  - deploy
  - build-electron
  - build-android


# Cache node-modules in-between jobs
# !!! This doesn't prevent from executing 'yarn install' for each stage in case the cache is not correctly restored !!!
cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - node-modules


# Pre job script
before_script:
  - echo "Start CI/CD"


# Build the SPA, compress and send it to the package repository.
# Also record 'dist/spa' as artifact for the 'build-webcontainer' stage
build-spa:
  stage: build-spa
  only:
    - master
  artifacts:
    paths:
      - dist/spa/
    expire_in: 1 day
  script:
    - yarn install
    - sed -i "s#http://localhost:8081#$API_BACKEND_URL#g" src/boot/axios.js
    - quasar build
    - cd dist && tar czf crash_course_quasar-spa.tar.gz spa/ --transform "s/spa/crash_course_quasar-spa/" && cd ..
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file dist/crash_course_quasar-spa.tar.gz "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/crash_course_quasar-spa/0.0.1/crash_course_quasar-spa.tar.gz"'


# Use the artifact 'dist/spa' from stage 'build-spa' and create a docker image to serve it. Send the image to the container repository
build-webcontainer:
  stage: build-webcontainer
  only:
    - master
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:latest


# Build the electron packaging for the 3 OS, compress and send it to the package repository
build-electron:
  stage: build-electron
  only:
    - master
  script:
    - yarn install
    - quasar build -m electron -T linux
    - cd dist/electron && tar czf crash_course_quasar-electron-linux.tar.gz "Paris sportif-linux-x64" --transform "s/Paris sportif-linux-x64/crash_course_quasar-electron-linux/" && cd ../..
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file dist/electron/crash_course_quasar-electron-linux.tar.gz "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/crash_course_quasar-electron/0.0.1/crash_course_quasar-electron-linux.tar.gz"'
    - quasar build -m electron -T win32
    - cd dist/electron && tar czf crash_course_quasar-electron-windows.tar.gz "Paris sportif-win32-x64" --transform "s/Paris sportif-win32-x64/crash_course_quasar-electron-windows/" && cd ../..
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file dist/electron/crash_course_quasar-electron-windows.tar.gz "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/crash_course_quasar-electron/0.0.1/crash_course_quasar-electron-windows.tar.gz"'
    - quasar build -m electron -T darwin
    - cd dist/electron && tar czf crash_course_quasar-electron-macos.tar.gz "Paris sportif-darwin-x64" --transform "s/Paris sportif-darwin-x64/crash_course_quasar-electron-macos/" && cd ../..
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file dist/electron/crash_course_quasar-electron-macos.tar.gz "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/crash_course_quasar-electron/0.0.1/crash_course_quasar-electron-macos.tar.gz"'



# Build the android app and send it to the package repository
build-android:
  stage: build-android
  only:
    - master
  script:
    - yarn install
    - ANDROID_SDK_ROOT="/usr/lib/android-sdk/" quasar build -m capacitor -T android
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file dist/capacitor/android/apk/release/app-release-unsigned.apk "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/crash_course_quasar-android/0.0.1/crash_course_quasar-android.apk"'


# Deploy the app to the kubernetes cluster configured in GitLab
deploy-dev:
  stage: deploy
  image: alpine
  environment:
    name: staging
  script:
    - mkdir $HOME/.kube
    - cp $KUBECONFIG $HOME/.kube/config
    - cat $HOME/.kube/config
    - apk update  && apk add --no-cache curl
    - curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
    - chmod +x ./kubectl && mv ./kubectl /usr/local/bin/kubectl
    - kubectl config set-context $(kubectl config current-context)
    - 'printf "apiVersion: v1\nkind: Secret\n$(kubectl create secret docker-registry gitlab-registry --docker-server=$CI_REGISTRY --docker-username=$GITLAB_REGISTRY_USER --docker-password=$GITLAB_REGISTRY_TOKEN --docker-email=$GITLAB_USER_EMAIL --docker-email=GITLAB_REGISTRY_EMAIL -o yaml --dry-run)" | kubectl apply -f -'
    - sed -i 's/_APP_NAME_/'"$CI_PROJECT_NAME"'/g; s/_VERSION_/'"$CI_COMMIT_SHA"'/g' k8s/deployment.yml;
    - sed -i 's/_APP_NAME_/'"$CI_PROJECT_NAME"'/g; s/_VERSION_/'"$CI_COMMIT_SHA"'/g' k8s/ingress.yml;
    - sed -i 's/_APP_NAME_/'"$CI_PROJECT_NAME"'/g; s/_VERSION_/'"$CI_COMMIT_SHA"'/g' k8s/service.yml;
    - kubectl apply -f k8s/deployment.yml
    - kubectl apply -f k8s/service.yml
    - kubectl apply -f k8s/ingress.yml
    - kubectl patch deployment $CI_PROJECT_NAME -p "{\"spec\":{\"template\":{\"metadata\":{\"labels\":{\"date\":\"`date +'%s'`\"}}}}}"
#  only:
#    - develop


# Post job script
after_script:
  - echo "End CI/CD"
